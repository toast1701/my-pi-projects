#encoding:UTF-8
import time
def check_input(check_bit,hex_value):
	bitwert=[1,2,4,8,16,32,64,128]
	int_status=hex_value
	in_status=int_status & bitwert[check_bit]
	in_status = False if in_status==bitwert[check_bit] else True
	return in_status
	
class window(object):
    def __init__(self, bus, name, i2c_adress, time_to_press, time_to_run, inp_up_bit, inp_down_bit, out_up_bit, out_down_bit, status):
		self.bus=bus
		self.name=name
		self.i2c_adress=i2c_adress
		self.time_to_press=time_to_press
		self.time_to_run=time_to_run
		self.inp_up_bit=inp_up_bit
		self.inp_down_bit=inp_down_bit
		self.out_up_bit=out_up_bit
		self.out_down_bit=out_down_bit
		self.input1=False
		self.input2=False
		self.status=status
		self.output1=True
		self.output2=True
		self.out1_merker=False
		self.out2_merker=False
		self.stop_time()
		self.akt_time=time.time()
		self.clock_set=False
		self.i=0
		self.j=0
    def read_input_2(self):
		self.input2=check_input(self.inp_down_bit,self.status)
    def read_input_1(self):
		self.input1=check_input(self.inp_up_bit,self.status)
    def set_out1(self,bool_val):
		self.output1=False if bool_val==1 else True		
    def set_out2(self,bool_val):
		self.output2=False if bool_val==1 else True
    def write_bits(self):
		bus=self.bus
		#read=bus.read_byte(int(self.i2c_adress,16)) 
		bitwert=[1,2,4,8,16,32,64,128]
		sum=0
		sum=sum + (bitwert[self.out_up_bit] if self.output1 else 0)
		sum=sum + (bitwert[self.out_down_bit] if self.output2 else 0)
		bits=255 ^ sum 
		bus.write_byte(int(self.i2c_adress,16),int(bits))
		#print self.name,bin(read)[2:].zfill(8),bin(sum)[2:].zfill(8),bin(~bits)[2:].zfill(8),self.old_inp1,self.old_inp2
    def read_bits(self):
		bus=self.bus
		self.status=bus.read_byte(int(self.i2c_adress,16))	
		self.read_input_1()
		self.read_input_2()
    def start_timer(self):
		self.clock_set=True
		self.akt_time=time.time()
    def reset_time(self):
		self.clock_set=True
		self.akt_time=time.time()
    def get_time(self):
		return time.time()-self.akt_time
    def stop_time(self):
		self.clock_set=False
		self.akt_time=time.time()		
    def check(self):
		self.old_inp1=self.input1
		self.old_inp2=self.input2
		self.read_bits()
		if (self.old_inp1!=self.input1 or self.old_inp2!=self.input2):
			if (self.get_time()>self.time_to_run):
				self.stop_time()
				self.set_out2(1)
				self.set_out1(1)
				self.out2_merker=False
				self.out1_merker=False
			if (self.input1 and self.input2==False):
				self.set_out2(1)
				self.set_out1(0)
				self.out1_merker=True
				if self.out2_merker: self.reset_time()
			if (self.input1==False and self.input2==False and self.get_time()<self.time_to_press):
				self.stop_time()
				self.clock_set=False
				self.set_out2(1)
				self.set_out1(1)
				self.out2_merker=False
				self.out1_merker=False
			if (self.input2 and self.input1==False):
				self.set_out1(1)
				self.set_out2(0) 
				self.out2_merker=True
				if self.out1_merker: self.reset_time()
			self.write_bits()
		#print "Ausgabe",self.i,self.j
	
		


	




